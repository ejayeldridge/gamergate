# Operation Disrespectful Nod

<div id="contents" style="visibility: hidden;"></div>
### Contents:

1. [Conduct Guide](#conduct-guide)
2. [Articles to Email](#email-the-articles)
3. [Who to Email](#to-their-respectives-site-39-s-advertisers-be-polite)
  1. [Polygon](#polygon)
  2. [Kotaku](#kotaku)
  3. [Rock Paper Shotgun](#rock-paper-shotgun)
  4. [Gamasutra](#gamasutra)
4. [Helpful Guides](#helpful-guides)


<div id="conduct-guide" style="visibility: hidden;"></div>
### [\[⇧\]](#contents) CONDUCT GUIDE

Just came here and not sure what to do?
- Read the following: https://archive.today/OFWFS then...
- Come back here for the full list of places to email

<div id="email-the-articles" style="visibility: hidden;"></div>
### [\[⇧\]](#contents) EMAIL THE ARTICLES
 
##### 'Gamers' don't have to be your audience. 'Gamers' are over. Exclusive - Leigh Alexander of Gamasutra
https://archive.today/l1kTW

##### We Might Be Witnessing The 'Death of An Identity' - Luke Plunkett of Kotaku
https://archive.today/YlBhH

##### The End of Gamers - Dan Golding
https://archive.today/L4vJG

##### A Guide to Ending "Gamers" - Devin Wilson of Gamasutra 
https://archive.today/2t93l

##### The death of the “gamers” and the women who “killed” them - Casey Johnson
https://archive.today/i928J

<div id="to-their-respectives-site-39-s-advertisers-be-polite" style="visibility: hidden;"></div>
### [\[⇧\]](#contents) TO THEIR RESPECTIVES SITE'S ADVERTISERS (BE POLITE)

<div id="polygon" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) POLYGON:

###### KRAFT

http://ir.kraftfoodsgroup.com/contactus.cfm - EMAIL FORM

###### SCOTTRADE (Pulled their ads, [Source](http://https://twitter.com/tacosforjustice/status/509487357817798659/photo/1))

https://www.scottrade.com/contact/contact-form.html

###### eHEALTH

https://www.ehealthinsurance.com/ehi/help/emailcontact?productType=IFP&contact=IFP

###### VERIZON

robert.a.varettoni@verizon.com < EXECUTIVE DIRECTOR OF MEDIA RELATIONS, SO BE FUCKING POLITE

raymond.mcconville@verizon.com < MANAGER OF MEDIA RELATIONS, SO STILL BE FUCKING POLITE

edward.s.mcfadden@verizon.com < EXECUTIVE DIRECTOR OF PUBLIC POLICY, YOU KNOW THE DRILL

###### SMITH'S, A DIVISION OF KROGER

https://www.kroger.com/topic/contact-us

------------
<div id="kotaku" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) KOTAKU:

###### PASTEBIN VERSION

http://pastebin.com/AAeBGdvA

###### STACKSOCIAL

support@stacksocial.com

###### IKOID

pr@ikoid.com

###### INDIE GAME

hello@dailyindiegame.com

###### NEWEGG

Wecare@newegg.com < CUSTOMER RELATIONS EMAIL.

###### ELECTRONIC ARTS (LOL)

CUSTOMERSERVICE@EA.COM

###### MONOPRICE

SUPPORT@MONOPRICE.COM

###### AMAZON

JEFF@AMAZON.COM - FUCKING CEO, BE POLITE

swheeler@amazon.com - DIRECTOR OF ADVERTISING AND PARTNER SHIPS

selipsky@amazon.com - VICE PRESIDENT

###### BEST BUY

laura.bishop@bestbuy.com - VICE PRESIDENT OF PUBLIC AFFAIRS

onlinestore@bestbuy.com

###### GAMESTOP

PublicRelations@GameStop.com

###### KROGER

keith.dailey@kroger.com - DIRECTOR OF MEDIA RELATIONS

###### Blizzard

pr@blizzard.com

rhilburger@blizzard.com - (Rob Hilburger VP of global communications, be polite!)

elrodriguez@blizzard.com - (Emil Rodriguez global public relations, be polite!)

###### ROCCAT

info@roccat.org

###### SPRINT

sprintcares@sprint.com

###### NINTENDO

nintendo@noa.nintendo.com

###### 2K GAMES

pr@2kgames.com

###### TURTLE BEACH

sales@turtlebeach.com

###### SAMSUNG

d2.cohen@sea.samsung.com - Danielle Meister Cohen, US CORPORATE MEDIA RELATIONS

###### SONY

marketinginquiries@sonyusa.com

###### TURBINE

pr@turbine.com

###### UBISOFT

support@ubisoft.com

michael.beadle@ubisoft.com - Michael Beadle, PR ASSOCIATE DIRECTOR 

###### AMC

amccustomerservice@amcnetworks.com

###### NCSOFT

csr@ncsoft.com ("Corporate Social Responsibilities") OR pr@ncsoft.com OR europeanpr@ncsoft.com

###### STATE FARM

holly.anderson.m3mj@statefarm.com - Holly Anderson, STATE FARM H.Q. MEDIA CONTACT

###### OLD SPICE (Procter & Gamble Company)

dicarlo.km@pg.com - Kate DiCarlo, PROCTER & GAMBLE MEDIA CONTACT

jessica.johnston@citizenrelations.com - Jessica Johnston, CITIZEN RELATIONS MEDIA CONTACT

###### SCION (Toyota Company)

Jmoreno@tma.toyota.com - Javier Moreno, TOYOTA MEDIA CONTACT

###### STARBUCKS

info@starbucks.com

###### CAPCOM

ir@capcom.co.jp (public relations office)

###### ENMASSE (MMORPG TERA)

brian@oneprstudio.com, Brian Alexander

jeane@oneprstudio.com, Jeane Wong 

matwood@enmasse.com, Matt Atwood 

aria@enmasse.com, Aria Brickner-McDonald

###### TRION WORLDS (ArcheAge, Rift, Defiance, Trove)

communications@trionworlds.com - (public relations office)

###### LIVESCRIBE

AmericasSales@livescribe.com OR International@livescribe.com OR CS@livescribe.com OR CS-uk@livescribe.com (UK) OR CS-au@livescribe.com (AU) ohen@sea.samsung.com

---------
<div id="rock-paper-shotgun" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) ROCK PAPER SHOTGUN:

###### NVIDIA 

mlim@nvidia.com - Micheal Lim, INDUSTRY ANALYST RELATIONS 

rsherbin@nvidia.com - Bob Sherbin, CORP. COMMUNICATIONS LEAD 

bdelrizzo@nvidia.com - Byran Del. Rizzo, GEFORCE & CONSUMER DESKTOP PRODUCTS 

###### KLEI ENTERTAINMENT (Devs of Don't Starve, Eets, Shank, Mark of the Ninja)

press@kleientertainment.com - PRESS

[@coreyrollins](https://twitter.com/CoreyRollins) - Corey Rollins, MARKETING/COMMUNITY MANAGER (twitter)

[@biiigfoot](https://twitter.com/biiigfoot) [@klei](https://twitter.com/klei) - Jamie Cheng, OWNER (twitter)

---------
<div id="gamasutra" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) GAMASUTRA:

###### INTEL (Pulled their ads, confirmed by Gamasutra)

Intel Corporate Responsibility Contact Form

https://www-ssl.intel.com/content/www/us/en/forms/corporate-responsibility-contact-us.html

###### UAT (University of Advance Tech)

bfabiano@uat.edu - Brian Fabiano, MARKETING STRATEGIC CREATIVE DIRECTOR

ahromas@uat.edu - Alan Hromas, MARKETING MANAGER

berickson@uat.edu - Bee Erickson, MARKETING EXECUTION COORIDINATOR

atreguboff@uat.edu - Aaron Treguboff, ONLINE TRAFFIC ANALYST

###### MAYA/AUTODESK

noah.cole@autodesk.com - Noah Cole, CORPORATE REPRESENTATIVE

---------

In addition, read [Part 1] and [Part 2] for more places to email to!

<div id="helpful-guides" style="visibility: hidden;"></div>
# [\[⇧\]](#contents) Helpful Guides
[Taco Justice](https://www.youtube.com/channel/UC2WMCIcPy1DfUz4jmkHAA4Q): #GAMERGATE NEEDS YOU TO EMAIL!
<a href="https://www.youtube.com/watch?v=kEpSXZ6vBN0" target="_blank"><img src="https://cdn.mediacru.sh/neOw3MAxbO8f.png"></a>

## Good Luck Anons!
![Screenshot](http://a.pomf.se/mfqqtv.gif)

[Part 1]:http://v.gd/N5DhWW
[Part 2]:http://v.gd/vvcnjJ
